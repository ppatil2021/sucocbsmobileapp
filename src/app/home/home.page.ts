import { Component } from '@angular/core';
import { Sim } from '@ionic-native/sim/ngx';
import * as cardValidator from 'card-validator';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private sim: Sim) { }

  ionViewDidEnter() {
    this.sim.getSimInfo().then(
      (info) => console.log('Sim info: ', info),
      (err) => console.log('Unable to get sim info: ', err)
    );
    this.sim.hasReadPermission().then(
      (info) => console.log('Has permission: ', info)
    );

    this.sim.requestReadPermission().then(
      () => console.log('Permission granted'),
      () => console.log('Permission denied')
    );
    // card-validator
    const options = {
      maxLength: 16,
    };
    const numberValidation = cardValidator.number('4111 1111 1111 1111', options);
    console.log('numberValidation *** ' + JSON.stringify(numberValidation));
    if (!numberValidation.isPotentiallyValid) {
      console.log('invalid card number');
    } else {
      console.log('valid card number');
    }
  }

}
