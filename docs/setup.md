nvm install v14.16.1 //node install
npm install -g @ionic/cli native-run cordova-res

* add android
npm install @capacitor/ios @capacitor/android
npx cap add android


* ionic build -- will create www folder in root directory
* npx cap sync android

* To open the project in Android Studio, run:
* Set path CAPACITOR_ANDROID_STUDIO in ~/.bashrc
- gedit ~/.bashrc

 * To get android studio path run --- [which android-studio]
- export CAPACITOR_ANDROID_STUDIO_PATH=/snap/bin/android-studio
- source ~/.bashrc
- npx cap open android

* To run on android 
- npx cap open android
- npx cap copy
- npx cap sync

*** To add plugin 

* Sim card 
npm install cordova-plugin-sim
npm install @ionic-native/sim
ionic cap sync

npm i @ionic-native/core

- add provider in `app.module.ts`  providers: [Sim] 

* To build on android
- ionic capacitor build android
