* Configure webpack 
- https://createapp.dev/webpack
npm install --save-dev webpack webpack-cli jest babel-jest babel-loader @babel/core @babel/preset-env 
html-webpack-plugin typescript ts-loader file-loader url-loader prettier

- Create webpack.config.js in the root and copy the contents of the generated file
- Create .babelrc in the root and copy the contents of the generated file
- Create folders src and dist and create source code files
- npm install husky --save-dev